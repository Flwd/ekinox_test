# -*- coding: utf-8 -*-
"""
Created on Thu May 20 15:02:44 2021

@author: legal
"""

#!/usr/bin/env python3

import unittest

from src.python import dead_or_alive

class DeadOrAliveTest(unittest.TestCase):
    
    def test_grid_cell_step(self):
        grid = ['##..#...##',
                '###...##..',
                '..#..#..##',
                '.#..#.##..']
        self.assertEqual(
            dead_or_alive.grid_cell_step(grid),
            ['#.#....##.',
              '#.##.###..',
              '#.##.#..#.',
              '.....####.']
        )


    def test_repeat_function(self):
        n = 5
        f = lambda x: x+1
        inp = 5
        self.assertEqual(
            dead_or_alive.repeat_function(n, inp, f),
            10
        )
        
        f = dead_or_alive.grid_cell_step
        inp = ['##..#...##',
                '###...##..',
                '..#..#..##',
                '.#..#.##..']
        self.assertEqual(
            dead_or_alive.repeat_function(n, inp),
            ['..........',
             '........#.',
             '..##.#...#',
             '..#######.']
        )
        
if __name__ == '__main__':
    unittest.main()