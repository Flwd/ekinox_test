docker build -t *image_name*:dev -f dockerfile.dev .

#Docker run
docker run -it \
	-p 8888:8888 -p 5000:5000 -p 6006:6006 --rm \
	-v //c/Users/Thibaud/Desktop/D2F/*repository_dir*:/home/app/ \
	-e DISPLAY=192.168.99.1:0 -v /tmp/.X11-unix:/tmp/.X11-unix \
	*image_name*:dev \
	jupyter notebook --ip=0.0.0.0 --allow-root &