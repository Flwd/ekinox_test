#!/usr/bin/env python3

from setuptools import setup, find_packages

import pathlib
here = pathlib.Path(__file__).parent.resolve()

setup(
    name='deadoralive',
    version='0.0.1',
    description='Un projet pour modéliser le comportement d une grille rectangulaire de cellules évoluant dans le temps',
    author='Thibaud LE GALL',
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Intended Audience :: Developers, biologists, chemists',
        'Topic :: cells evolution',
        'Programming Language :: Python :: 3',
    ],
    packages=find_packages(),
    python_requires='>=3.0, <4',
    entry_points={
        'console_scripts': [
            'deadoralive=src.main:main',
        ],
    },
)