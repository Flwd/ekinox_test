# -*- coding: utf-8 -*-
"""
Created on Thu May 20 10:37:05 2021

@author: legal
"""


def grid_cell_step(cells_grid):
    '''Takes in a grid of cells at time t, returns the grid of cells at t+1'''

    moves = [(0, 1), (0, -1), (1, 0), (-1, 0),
             (1, 1), (-1, 1), (1, -1), (-1, -1)]
    new_grid = []

    for i, line in enumerate(cells_grid):
        new_line = ''
        for j, elem in enumerate(line):

            adjacent_cells=0
            for m_y, m_x in moves:
                if 0<=i+m_x<len(cells_grid) \
                and 0<=j+m_y<len(cells_grid[0]):
                    adjacent_cells += cells_grid[i+m_x][j+m_y]=='#'

            if elem=='#':
                if adjacent_cells<2 or adjacent_cells>3:
                    new_line += '.'
                else:
                    new_line += '#'
            else:
                if adjacent_cells==3:
                    new_line += '#'
                else:
                    new_line += '.'
        new_grid.append(new_line)

    return new_grid


def repeat_function(nb_of_application, input_obj, function=grid_cell_step):
    '''Takes in a number n, a function and an input, returns the result of
    the appliance of the fonction to the input n times'''
    if nb_of_application > 0:
        return repeat_function(nb_of_application-1, function(input_obj),
                               function)
    return input_obj


if __name__ == "__main__":
    pass

