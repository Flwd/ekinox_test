# -*- coding: utf-8 -*-
"""
Created on Thu May 20 16:48:14 2021

@author: legal
"""

import sys
import argparse
import src.python.dead_or_alive as doa


def arg_parser(args):
    parser = argparse.ArgumentParser('Parsing training arguments')
    parser.add_argument('--input_file_path', type=str, \
                        help='The path to the file with a grid of cells')
    parser.add_argument('--number_of_epoch', type=int, \
                help='The number of epoch to perform')
    return parser.parse_args(args)


def grid_file_parser(file_path):
    '''Takes a path to a file, returns a grid of cells'''
    with open(file_path, 'r') as f:
        grid = f.read().split("\n")
    return grid


def main():
    args = arg_parser(sys.argv[1:])
    print(doa.repeat_function(args.number_of_epoch,
                        grid_file_parser(args.input_file_path)))
    

if __name__ == "__main__":
    args = arg_parser(sys.argv[1:])
    main(args)