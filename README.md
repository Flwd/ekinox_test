# DeadOrAlive

Le but du projet est de modéliser le comportement d'une grille rectangulaire de cellules évoluant dans le temps

You need Python3.x on you machine to setup the project

To install you can use the command:

`python setup.py install`

from this repository. It will install the command deadoralive

For testing purposes, there is a dockerfile that can be found in env/test providing a specificqtion for a virtual environment on docker